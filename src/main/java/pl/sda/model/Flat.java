package pl.sda.model;

import java.time.LocalDate;

public class Flat {
    private final double space;
    private final String owner;
    private final LocalDate buildDate;

    public Flat(double space, String owner, LocalDate buildDate) {
        this.space = space;
        this.owner = owner;
        this.buildDate = buildDate;
    }

    public double getSpace() {
        return space;
    }

    public String getOwner() {
        return owner;
    }

    public LocalDate getBuildDate() {
        return buildDate;
    }
}
