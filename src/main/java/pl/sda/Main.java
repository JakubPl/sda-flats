package pl.sda;

import pl.sda.model.Flat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    private static final String JANUSZ_JANUSZEWSKI = "Janusz Januszewski 2";
    private static final String JANUSZ_FILIPIAK = "Janusz Filipiak";
    private static final Path PATH_TO_CSV = Paths.get("mieszkania.csv");
    private static final List<Flat> flats = new LinkedList<>();

    static {
        flats.add(new Flat(50.4, "Jan Kowalski", LocalDate.of(1999, 1, 1)));
        flats.add(new Flat(28.4, JANUSZ_JANUSZEWSKI, LocalDate.of(2005, 10, 10)));
        flats.add(new Flat(21.6, JANUSZ_JANUSZEWSKI, LocalDate.of(2005, 12, 12)));
        flats.add(new Flat(5.4, JANUSZ_FILIPIAK, LocalDate.of(1999, 4, 5)));
        flats.add(new Flat(10.0, JANUSZ_FILIPIAK, LocalDate.of(2000, 1, 1)));
        flats.add(new Flat(4.6, JANUSZ_FILIPIAK, LocalDate.of(2010, 3, 2)));

    }

    public static void main(String[] args) throws IOException {
        printProperties(flats);
        //czytanie z pliku
        System.out.println("Czytam wlasciwosci z pliku..");
        List<String> linesInFile = Files.readAllLines(PATH_TO_CSV);
        final List<Flat> flatsFromFile = linesInFile.stream()
                .map(line -> line.split(";"))
                .map(Main::mapToFlat)
                .collect(Collectors.toList());
        printProperties(flatsFromFile);
    }

    private static void printProperties(List<Flat> flatsToProcess) {
        System.out.println("Wlasciciel najstarszego mieszkania:");
        flatsToProcess.stream()
                .max(Comparator.comparing(Flat::getBuildDate))
                .ifPresent(oldestFlat -> System.out.println(oldestFlat.getOwner()));

        System.out.println("Sumaryczna powiechnia mieszkan Janusza Filipiaka:");
        final double filipiakSpaceSum = flatsToProcess.stream()
                .filter(flat -> JANUSZ_FILIPIAK.equals(flat.getOwner()))
                .mapToDouble(e -> e.getSpace())
                .sum();
        System.out.println(filipiakSpaceSum);

        System.out.println("Sumaryczna powiechnia mieszkan poszczegolnych wlascicieli:");
        flatsToProcess.stream()
                .collect(Collectors.groupingBy(Flat::getOwner, Collectors.summingDouble(Flat::getSpace)))
                .forEach((owner, spaceSum) -> System.out.println(owner + " ma " + spaceSum + "m2"));
    }

    private static Flat mapToFlat(String[] columns) {
        // 10.12.2011
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return new Flat(
                Double.valueOf(columns[1]),
                columns[0],
                LocalDate.parse(columns[2], formatter));
    }
}
